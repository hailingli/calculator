/**
 * 
 */
package edu.hawaii.ics211;

/**
 * The test for the Calculator class
 * @author Hailing Li
 *
 */
public class TestCalculator {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
			Calculator a= new Calculator();
			System.out.println(a.subtract(2,3));
			System.out.println(a.add(2,3));
			System.out.println(a.multiply(2,3));
			System.out.println(a.divide(2,3));
			System.out.println(a.modulo(2,3));
			System.out.println(a.pow(2,3));

	}

}
